var Job = function(){

	this.Job = function(onetsoc_code, title, description){
		this.onetsoc_code = onetsoc_code;
		this.title = title;
		this.description = description;

		return this;
	}
	
};

module.exports = Job;