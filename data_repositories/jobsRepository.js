var Connection = require('tedious').Connection;
var Request = require('tedious').Request;  
var dbHelper = require('../utilities/dbHelper');
var jobModel = require('../models/job')

var jobsRepository = function(){
   var db = new dbHelper();

   this.getJobs = function(page, size, q, onSuccess){

        var defaultPage = 1;
        var defaultSize = 10;

        if(page){ defaultPage = page; }
        if(size){ defaultSize = size; }

        var numToRetrieve = defaultSize * defaultPage;
        var query = "SELECT TOP " + numToRetrieve + "* FROM occupation_data WHERE title LIKE '%" + q + "%'";

        db.connect( function(connection){
            var request = new Request(query, 
                function(err) {  
                    if (err) {  
                        console.log(err);}  
                    });  
            
            var models = []; 
            
            request.on('row', function(columns) { 
                
                var code = columns[0].value;
                var title = columns[1].value;
                var desc = columns[2].value;

                var jobClass = new jobModel();
                var model = jobClass.Job(code, title, desc);
                models.push(model);
            });  
            
            request.on('doneProc', function(rowCount, more, rows) { //NOTE - doneProc used insteal of 'done'. done event was not working.
                onSuccess(models);
            });  
            
            connection.execSql(request); 
        });
    };

}

module.exports = jobsRepository;

