var Connection = require('tedious').Connection;

var db = function(){
    
    //TODO - Move configuration to JSON Config file.
    var config = {
        userName: 'shivendra',
        password: 'experiment1#',
        server: 'experiment.database.windows.net',
        options: {encrypt: true, database : 'experimentdb'}
    };

    this.connect = function(successCb){
    	try{
	    	var connection = new Connection(config);

	    	connection.on('connect', function(err) {  

	        	successCb(connection); 
			}); 
    	}
    	catch(err){
			console.log(err);
			//TODO: Handle and Log Error 
    	}
    };
};

module.exports = db;