var Joi = require('joi');
var jobsRepository = require('../data_repositories/jobsRepository');

var apiEndpoint = '/findly-interest/api';

var jobRoutes = [

	{
	    method: 'GET',
	    path: apiEndpoint + '/jobs/{page}/{size}/{q}', 
	    handler: function (request, reply) {
	    	var repo = new jobsRepository();

			repo.getJobs(request.params.page, request.params.size, request.params.q,
				function(jobs){
					var result = {
						'data' : jobs,
						'page' : request.params.page,
						'size' : request.params.size,
						'total': jobs.length
					};

					return reply(result);
				}
			);
	    },
	    config : {
	    	validate : {
	    		params : {
	    			q : Joi.string().min(3),
	    			page : Joi.number(),
	    			size : Joi.number()
	    		}
	    	}
	    }
	}

];


module.exports = jobRoutes;

