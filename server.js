'use strict';

const Hapi = require('hapi');
var jobRoutes = require('./routes/jobRoutes');
var Connection = require('tedious').Connection;


var server = new Hapi.Server();

server.connection({
  host: '0.0.0.0',
  port: process.env.PORT || 8080
});

server.route(jobRoutes);

server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});




